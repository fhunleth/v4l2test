# -------------------------------------------------
# Project created by QtCreator 2011-09-19T06:48:47
# -------------------------------------------------
TARGET = v4l2test
TEMPLATE = app
SOURCES += main.cpp \ 
    framecapturer.cpp \
    videoframe.cpp \
    davinciresizer.cpp \
    davincipreviewer.cpp \
    h264encoder.cpp \
    tiutilities.cpp
HEADERS += \ 
    framecapturer.h \
    videoframe.h \
    davinciresizer.h \
    davincipreviewer.h \
    h264encoder.h \
    tiutilities.h
FORMS += 
LIBS += 

INCLUDEPATH += media_davinci

# Run configuro to generate the compiler and linker flags for pulling
# in the TI libraries. This gets run once when running qmake.
system(./runconfiguro.sh) {
    message("Configuro succeeded. Rerun qmake if you need to change tilibs_dm365.cfg")
} else {
    error("Something went wrong with configuro!")
}

include(tilibs_dm365/compiler.qmake)
include(tilibs_dm365/linker.qmake)

target.path = /usr/bin
INSTALLS += target

OTHER_FILES +=
