#include "davinciresizer.h"

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <media/davinci/imp_resizer.h>
#include <media/davinci/dm365_ipipe.h>


#include <QDebug>

DavinciResizer::DavinciResizer() :
    rsz_fd(-1),
    rotated180_(false)
{
}

DavinciResizer::~DavinciResizer()
{
    close();
}

void DavinciResizer::setOrientation(bool rotated180)
{
    if (rsz_fd != -1)
        qFatal("DavinciResizer::setOrientation called when running");

    rotated180_ = rotated180;
}


void DavinciResizer::commitSettingsAndStart()
{
    qDebug("opening resize device");
    rsz_fd = open("/dev/davinci_resizer", O_RDWR);
    if (rsz_fd <= 0)
            qFatal("Cannot open resize device ");

    unsigned int user_mode = IMP_MODE_CONTINUOUS;
    if (ioctl(rsz_fd, RSZ_S_OPER_MODE, &user_mode) < 0)
            qFatal("Can't set operation mode");

    unsigned int oper_mode;
    if (ioctl(rsz_fd, RSZ_G_OPER_MODE, &oper_mode) < 0)
            qFatal("Can't get operation mode");

    if (oper_mode == user_mode)
            qDebug("Successfully set mode to continuous in resizer");
    else
            qFatal("failed to set mode to continuous in resizer");

    // set configuration to chain resizer with preview    
    rsz_channel_config rsz_chan_config;
    rsz_continuous_config rsz_cont_config; // continuous mode
    rsz_chan_config.oper_mode = IMP_MODE_CONTINUOUS;
    rsz_chan_config.chain  = 1;
    rsz_chan_config.len = 0;
    rsz_chan_config.config = NULL; /* to set defaults in driver */
    if (ioctl(rsz_fd, RSZ_S_CONFIG, &rsz_chan_config) < 0)
            qFatal("Error in setting default configuration in resizer");

    qDebug("default configuration setting in Resizer successfull");
    bzero(&rsz_cont_config, sizeof(struct rsz_continuous_config));
    rsz_chan_config.oper_mode = IMP_MODE_CONTINUOUS;
    rsz_chan_config.chain = 1;
    rsz_chan_config.len = sizeof(struct rsz_continuous_config);
    rsz_chan_config.config = &rsz_cont_config;

    if (ioctl(rsz_fd, RSZ_G_CONFIG, &rsz_chan_config) < 0)
            qFatal("Error in getting resizer channel configuration from driver");

    // we can ignore the input spec since we are chaining. So only
    // set output specs
    rsz_cont_config.output1.enable = 1;
    rsz_cont_config.output2.enable = 0;

    unsigned char flipValue = rotated180_ ? 1 : 0;
    rsz_cont_config.output1.h_flip = flipValue;
    rsz_cont_config.output1.v_flip = flipValue;
#if 0
    rsz_cont_config.output2.width = destinationRect_.width();
    rsz_cont_config.output2.height = destinationRect_.height();
    rsz_cont_config.output2.source_x = sourceRect_.x();
    rsz_cont_config.output2.source_y = sourceRect_.y();
    rsz_cont_config.output2.source_width = sourceRect_.width();
    rsz_cont_config.output2.source_height = sourceRect_.height();
    rsz_cont_config.output2.pix_fmt = IPIPE_YUV420SP;
#endif

    rsz_chan_config.len = sizeof(struct rsz_continuous_config);
    rsz_chan_config.config = &rsz_cont_config;
    if (ioctl(rsz_fd, RSZ_S_CONFIG, &rsz_chan_config) < 0)
        qFatal("Error in setting configuration in resizer");

    qDebug("Resizer initialized");
}

void DavinciResizer::close()
{
    if (rsz_fd == -1) {
        ::close(rsz_fd);
        rsz_fd = -1;
    }
}
