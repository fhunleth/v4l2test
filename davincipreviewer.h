#ifndef DAVINCIPREVIEWER_H
#define DAVINCIPREVIEWER_H

class DavinciPreviewer
{
public:
    DavinciPreviewer();
    ~DavinciPreviewer();

    void init();

    void close();

private:
    int preview_fd;

};

#endif // DAVINCIPREVIEWER_H
