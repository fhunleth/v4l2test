#include "videoframe.h"

VideoFrame::VideoFrame() :
    capturer(0),
    id(0),
    width(0),
    height(0),
    luminance(0),
    luminanceSize(0),
    chrominance(0),
    chrominanceSize(0)
{
}
