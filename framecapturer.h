#ifndef FRAMECAPTURER_H
#define FRAMECAPTURER_H

#include <QObject>
#include <QSize>
#include <QRect>

#include "davincipreviewer.h"
#include "davinciresizer.h"
#include "videoframe.h"

class QSocketNotifier;
class QThread;

struct FrameCaptureSettings {
    bool rotated180;
    QRect sourceRect;
    QSize encodeDimensions;
};

class FrameCapturer : public QObject
{
    Q_OBJECT
public:

    explicit FrameCapturer(const QByteArray &cameraName, QObject *parent = 0);
    void allocateCmemBuffers();

    void stopCapturing();

    // Getters and setters - valid after call to startCapturing()
    QSize maxDimensions() const;
    int captureBytesPerLine() const;
    QSize encodeDimensions() const;

    void setOrientation(bool rotated180);
    void setSourceRect(const QRect &rect);

    static FrameCapturer *startInThread(const QByteArray &cameraName, QThread *thread);

public slots:
    void changeSettings(const FrameCaptureSettings &newSettings);

signals:
    void settingsChanged(FrameCaptureSettings settings);
    void frameReady(VideoFramePtr frame);

private slots:
    void startCapturing();
    void processFrame();
    void frameDone(VideoFrame *frame);

private:
    void set_data_format();
    void initCaptureBuffers();
    int start_capture_streaming();
    void init_camera_capture();

    void cleanup_capture();

    static void frameDoneCallback(VideoFrame *frame);

private:
    QByteArray cameraName_;

    QSocketNotifier *notifier_;

    DavinciResizer resizer_;
    DavinciPreviewer previewer_;

    int capt_fd;
    unsigned int frameCnt;
    unsigned char* dummy;

    int framesOutToEncoder_;

    QSize maxDimensions_;
    QSize encodeDimensions_;
    int captureBytesPerLine_;
};

// It really stinks that we have to hardcode
// the values here. Sometime clean this up
// and have them queried from the camera drivers.
//#define COLOR_CAMERA_WIDTH 928
//#define COLOR_CAMERA_HEIGHT 576
#define COLOR_CAMERA_WIDTH 1280
#define COLOR_CAMERA_HEIGHT 720

#define FRAME_CAPTURE_MAX_WIDTH COLOR_CAMERA_WIDTH
#define FRAME_CAPTURE_MAX_HEIGHT COLOR_CAMERA_HEIGHT


#endif // FRAMECAPTURER_H
