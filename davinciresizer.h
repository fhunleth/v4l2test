#ifndef DAVINCIRESIZER_H
#define DAVINCIRESIZER_H

class DavinciResizer
{
public:
    DavinciResizer();
    ~DavinciResizer();

    void setOrientation(bool rotated180);

    void commitSettingsAndStart();

    void close();

private:
    int rsz_fd;

    bool rotated180_;
};

#endif // DAVINCIRESIZER_H
