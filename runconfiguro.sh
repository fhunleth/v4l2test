#!/bin/sh

#
# Run the TI configuro utility to generate compile and link commands
# for using the TI codecs.
#

[ "$BUILDROOT_BASE" ] || BUILDROOT_BASE=`pwd`/../buildroot

TOOLCHAIN_ROOT=$BUILDROOT_BASE/output/host
STAGING_ROOT=$BUILDROOT_BASE/output/staging
TARGET_CROSS=$TOOLCHAIN_ROOT/usr/bin/arm-none-linux-gnueabi-

TI_PLATFORM=dm365
XDC_PLATFORM=ti.platforms.evmDM365
XDC_TARGET=gnu.targets.arm.GCArmv5T
XDC_CONFIG_BASENAME=tilibs_$TI_PLATFORM

TI_INSTALL=$STAGING_ROOT/usr/share/ti
XDC_INSTALL_DIR=$TI_INSTALL/ti-xdctools-tree
FC_INSTALL_DIR=$TI_INSTALL/ti-framework-components-tree
CE_INSTALL_DIR=$TI_INSTALL/ti-codec-engine-tree
CMEM_INSTALL_DIR=$TI_INSTALL/ti-linuxutils-tree
CODEC_INSTALL_DIR=$TI_INSTALL/ti-codecs
XDAIS_INSTALL_DIR=$TI_INSTALL/ti-xdais-tree
MVTOOL_DIR=$TOOLCHAIN_ROOT/usr

# XDC Configuration
CONFIGURO="$XDC_INSTALL_DIR/xs xdc.tools.configuro"
XDC_PATH=".;$XDC_INSTALL_DIR/packages;$FC_INSTALL_DIR/packages;$CE_INSTALL_DIR/packages;$XDAIS_INSTALL_DIR/packages;$CODEC_INSTALL_DIR/packages;$CMEM_INSTALL_DIR/packages;$LPM_INSTALL_DIR/packages"

PLATFORM_XDC="$XDC_PLATFORM" CROSS_COMPILE=$TARGET_CROSS XDCPATH="$XDC_PATH" $CONFIGURO -c $MVTOOL_DIR -o $XDC_CONFIG_BASENAME -t $XDC_TARGET -p $XDC_PLATFORM -b config.bld $XDC_CONFIG_BASENAME.cfg

cat $XDC_CONFIG_BASENAME/compiler.opt | xargs ./configuro2qmake.sh > $XDC_CONFIG_BASENAME/compiler.qmake
gawk '/^    / {print "LIBS +=",$1}' $XDC_CONFIG_BASENAME/linker.cmd > $XDC_CONFIG_BASENAME/linker.qmake

