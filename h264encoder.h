#ifndef H264ENCODER_H
#define H264ENCODER_H

#include <QObject>
#include "videoframe.h"

class QThread;
struct H264EncoderPrivate;

class H264Encoder : public QObject
{
    Q_OBJECT
public:
    H264Encoder(int maxWidth, int maxHeight, int maxBitrate, QObject *parent = 0);
    ~H264Encoder();

    static H264Encoder *startInThread(int maxWidth, int maxHeight, int maxBitrate, QThread *thread);

    void initEncoder();
    void freeEncoder();

signals:
    
public slots:
    void frameReady(const VideoFramePtr &frame);

private:
    const int maxWidth_;
    const int maxHeight_;
    const int maxBitrate_;
    int bitRate_;

    H264EncoderPrivate *priv_;
};

#endif // H264ENCODER_H
