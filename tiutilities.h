#ifndef TIUTILITIES_H
#define TIUTILITIES_H

#include <QString>

QString TIErrorToString(int errorCode);

#endif // TIUTILITIES_H
