#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include <QSharedPointer>
#include <linux/videodev2.h>

class FrameCapturer;

class VideoFrame
{
public:
    VideoFrame();

public:
    v4l2_buffer v4l2buf;
    FrameCapturer *capturer;

    int id;
    int width;
    int height;

    unsigned char *luminance;
    int luminanceSize;

    unsigned char *chrominance;
    int chrominanceSize;

    int totalFrameSize;
//    unsigned char *scaledFrame;
};

typedef QSharedPointer<VideoFrame> VideoFramePtr;

#endif // VIDEOFRAME_H
