#include "h264encoder.h"
#include "videoframe.h"
#include "tiutilities.h"

#include <xdc/std.h>
#include <ti/sdo/ce/osal/Global.h>
#include <ti/sdo/ce/osal/Memory.h>
#include <ti/sdo/ce/osal/Sem.h>
#include <ti/sdo/utils/trace/gt.h>

#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/video1/videnc1.h>
#include <ti/sdo/codecs/h264enc/ih264venc.h>
#include <ti/sdo/linuxutils/cmem/include/cmem.h>

#define SAVE_TO_FILE

struct H264EncoderPrivate
{
    Engine_Handle hEngine;
    VIDENC1_Handle hEncode;
    IH264VENC_DynamicParams dynParams;

    XDAS_Int32 encoderOutBufSizeArray[2];
    XDAS_Int8* encoderOutBufArray[2];
    XDM_BufDesc encoderOutBufDesc;

#ifdef SAVE_TO_FILE
    FILE *encodeOutFile;
#endif
};

H264Encoder::H264Encoder(int maxWidth, int maxHeight, int maxBitrate, QObject *parent) :
    QObject(parent),
    maxWidth_(maxWidth),
    maxHeight_(maxHeight),
    maxBitrate_(maxBitrate)
{
    priv_ = new H264EncoderPrivate();
#ifdef SAVE_TO_FILE
    priv_->encodeOutFile = fopen("encode.h264", "wb");
#endif
}

H264Encoder::~H264Encoder()
{
    delete priv_;
}

H264Encoder *H264Encoder::startInThread(int maxWidth, int maxHeight, int maxBitrate, QThread *thread)
{
    H264Encoder *encoder = new H264Encoder(maxWidth, maxHeight, maxBitrate);
    encoder->moveToThread(thread);
    encoder->initEncoder();

    return encoder;
}

void H264Encoder::initEncoder()
{
    static char *engineName = strdup("encode");
    static char *codecName = strdup("h264enc");

    qDebug("Calling Engine_open");
    priv_->hEngine = Engine_open(engineName, NULL, NULL);
    if (priv_->hEngine == NULL)
        qFatal("Failed to open codec engine: %s", engineName);

    //static params
    IH264VENC_Params params;
    params.videncParams.size = sizeof(params);
    params.videncParams.encodingPreset = XDM_HIGH_SPEED; //HIGH_SPEED, HIGH_QUALITY, USER_DEFINED
    params.videncParams.rateControlPreset = IVIDEO_LOW_DELAY;  //LOW_DELAY, STORAGE, TWOPASS, NON, USER_DEFINED
    params.videncParams.maxHeight = maxHeight_;  //Maximum height
    params.videncParams.maxWidth = maxWidth_;    //Maximum width;
    params.videncParams.maxFrameRate = 60000;  //Maximum frame rate in fps * 1000.
    params.videncParams.maxBitRate = maxBitrate_;  //Maximum bit rate, bits per second.
    params.videncParams.dataEndianness = XDM_BYTE;  //XDM_BYTE, XDM_LE_16, XDM_LE_32 = 3
    params.videncParams.maxInterFrameInterval = 1;  //I to P frame distance. e.g. = 1 if no B frames
    params.videncParams.inputChromaFormat = XDM_YUV_420SP;  //XDM_YUV_420SP, XDM_YUV_420P, XDM_YUV_422ILE
    params.videncParams.inputContentType = IVIDEO_PROGRESSIVE; //IVIDEO_PROGRESSIVE, IVIDEO_INTERLACED
    params.videncParams.reconChromaFormat = XDM_CHROMA_NA;  //XDM_CHROMA_NA, XDM_YUV_420P
    params.profileIdc = 77;     /*!< ofile IDC (66=baseline, 77=main, 88=extended, 100=HighProfile) */
    params.levelIdc = IH264VENC_LEVEL_40;       /*!< level idc */
    params.meAlgo = 0;            /*!< 0=Normal Search, 1=Improved Video quality at low bit rate */
    params.enableVUIparams = 0;/*!< Enable VUI Parameters */
    params.entropyMode = 0;    /*!< entropy mode flag 0-CAVLC, 1-CABAC */
    params.transform8x8FlagIntraFrame = 0; /*!< Flag for 8x8 Transform in Inter frames*/
    params.transform8x8FlagInterFrame = 0; /*!< Flag for 8x8 Transform in Intra Frames*/
    params.seqScalingFlag = 0; /*!< Sequence Scaling Matrix Present Flag */
                                /*!< = Disable, 1 = Auto, 2 = Low, 3 = Moderate, 4 = Reserved */
    params.encQuality = 2;     /*!< 0 => version 1.1 backward compatible mode, 2 => Platinum mode,
                                     1 => Full feature, high Quality (It is depreciated due to performance reasons) */
    params.enableARM926Tcm = 1;  /*!< 0 -> do not use ARM926 TCM, 1 -> use ARM926 TCM */
    params.enableDDRbuff = 0;    /*!< 0 -> do not use DDR, 1 -> use DDR instead of IMCOP */
    params.sliceMode = 0;        /*!< 0 -> no multiple slices, 1 -> multiple slices - bits/slice  */
                                  /*!< 2 -> multiple slices-MBs/slice,3 -> multiple slices - Rows/slice*/
    params.outputDataMode = 1;    /* 0 -> low latency, encoded streams produced after N (configurable) slices encode,*/
                                   /* 1 -> encoded stream produce at the end of frame */
    params.sliceFormat = 1;      /* 0 -> encoded stream in NAL unit format, */
                                  /* 1 -> encoded stream in bytes stream format */
    params.numTemporalLayers = 0; /* Number of temporal layers
                                        0 -> one layer (Base layer)
                                        1 -> two layers
                                        2 -> three layers
                                        3 -> four layers
                                        255 -> Chain free P frames */
    params.svcSyntaxEnable = 0;   /* SVC and MMCO enable/disable
                                      0 -> SVC disabled short term ref frames used
                                      1 -> SVC enabled short term ref frames used
                                      2 -> SVC disabled long term ref frames used
                                      3 -> SVC enabled long term ref frames used*/
    params.EnableLongTermFrame = 0;/* Flag to enable/disable long term reference frame */
    params.ConstraintSetFlag = 0; /*!< ConstraintSetFlags */
    params.Log2MaxFrameNumMinus4 = 0; /*!< Set the maximum frame number value  */
    //dynamic params
    priv_->dynParams.videncDynamicParams.size = sizeof(priv_->dynParams);
    priv_->dynParams.videncDynamicParams.inputHeight = maxHeight_; //Input frame height
    priv_->dynParams.videncDynamicParams.inputWidth = maxWidth_; //Input frame width
    priv_->dynParams.videncDynamicParams.refFrameRate = 30000;  //Reference, or input, frame rate in  fps * 1000
    priv_->dynParams.videncDynamicParams.targetFrameRate = 30000;  //Target frame rate in fps * 1000
    priv_->dynParams.videncDynamicParams.targetBitRate = bitRate_;   //Target bit rate in bits per second.
    priv_->dynParams.videncDynamicParams.intraFrameInterval = 30;  //The number of frames between two I frames
    priv_->dynParams.videncDynamicParams.generateHeader = XDM_ENCODE_AU; //Encode entire access unit, including the headers
    priv_->dynParams.videncDynamicParams.captureWidth = maxWidth_;  //DEFAULT(0): use imagewidth as pitch else use given capture  width for pitch provided it is greater than image width.
    priv_->dynParams.videncDynamicParams.forceFrame = IVIDEO_NA_FRAME;  //Force the current (immediate) frame to be encoded as a specific frame type. (NA/I/IDR/P/B)
    priv_->dynParams.videncDynamicParams.interFrameInterval = 0;  //Number of B frames between two reference frames
    priv_->dynParams.videncDynamicParams.mbDataFlag = 0;  //Flag to indicate that the algorithm should use MB data
    priv_->dynParams.intraFrameQP = 32;     /*!< Quant. param for I Slices (0-51) */
    priv_->dynParams.interPFrameQP = 32;    /*!< Quant. Param for non - I Slices  */
    priv_->dynParams.initQ = 32;           /*!< Initial QP for RC */
    priv_->dynParams.rcQMax = 51;          /*!< Maximum QP to be used  Range[0,51] */
    priv_->dynParams.rcQMin = 0;          /*!< Minimum QP to be used  Range[0,51] */
    priv_->dynParams.rcQMaxI = 51;          /*!< Maximum QP to be used  Range[0,51] */
    priv_->dynParams.rcQMinI = 0;          /*!< Minimum QP to be used  Range[0,51] */
    priv_->dynParams.airRate = 0;          /*!< Adaptive Intra Refresh MB Period */
    priv_->dynParams.sliceSize = 1460;        /*!< No. of bytes per slice or No. of MB rows per Slice */
    priv_->dynParams.lfDisableIdc = 0;     /*!< Loop Filter enable/disable control */
    priv_->dynParams.rcAlgo = 1;           /*!< Algorithm to be used by Rate Ctrl Scheme*/
                                 /*!< => CBR, 1 => VBR, 2 => Fixed QP, 3=> CVBR,*/
                                 /*!< 4=> FIXED_RC 5=> CBR custom1 6=> VBR custom1*/
    priv_->dynParams.maxDelay = 2000;         /*!< max delay for rate control interms of ms,*/
                                 /*!< set it to 1000 for 1 second delay  */
    priv_->dynParams.aspectRatioX = 1;     /*!< X scale for Aspect Ratio */
    priv_->dynParams.aspectRatioY = 1;      /*!< Y scale for Aspect Ratio */
    priv_->dynParams.enableBufSEI = 0;     /*!< Enable Buffering period SEI */
    priv_->dynParams.enablePicTimSEI = 0;  /*!< Enable Picture Timing SEI */
    priv_->dynParams.perceptualRC = 1;     /*!< Flag for enabling/disabling PRC */
    priv_->dynParams.idrFrameInterval = 0;  /* IDR Frame Interval */
    priv_->dynParams.mvSADoutFlag = 0;      /* Flag for enable/disable MVSAD out to the app*/
    priv_->dynParams.resetHDVICPeveryFrame = 0; /* Flag for resetting hdvicp */
                                      /* after every frame encode */
    priv_->dynParams.enableROI = 0;      /*!< 0 -> do not enable ROI, 1 -> enable ROI */
    priv_->dynParams.metaDataGenerateConsume = 0; /* Flag to indicate Generate or Consume metaData*/
    priv_->dynParams.maxBitrateCVBR = 768000;  /* Specifies the max Bitrate for CVBR RC algortihm*/
    priv_->dynParams.maxHighCmpxIntCVBR = 0;    /* Specifies the maximum duration of increased complexity */
    priv_->dynParams.CVBRsensitivity = 0;   /* Specifies the target bitrate used by rate control*/
                                   /* in high complexity state */
    priv_->dynParams.LBRmaxpicsize = 0;     /* parameter controls the maximum number of bits consumed per frame*/
    priv_->dynParams.LBRminpicsize = 0;     /* parameter controls the minimum number of bits consumed per frame */
    priv_->dynParams.LBRskipcontrol = 0x00050004;    /* parameter configures the minimum number of frames */
                                   /*  to be encoded in a set of N frames */
    priv_->dynParams.interlaceRefMode = 0;      /*!<Mode to choose between ARF/SPF/MRCF*/
                                      /*!< 0 = ARF (default mode), 1 = SPF, 2 = MRCF */
    priv_->dynParams.LongTermRefreshInterval = 0;/* Refresh period for long term reference frame */
    priv_->dynParams.UseLongTermFrame = 0;    /* Use longterm frame as reference*/
    priv_->dynParams.SetLongTermFrame = 0;    /* Set a frame as longterm frame for reference */
    priv_->dynParams.VUI_Buffer = &H264VENC_TI_VUIPARAMBUFFER;     /* Pointer to VUI buffer*/
    priv_->dynParams.CustomScaleMatrix_Buffer = &H264VENC_TI_CUSTOMSCALINGMATRIX; /* CUSTOMSCALINGMATRIX*/
    priv_->dynParams.enableGDR = 0;             /* Flag to enable Gradual Decoder Refresh */
    priv_->dynParams.GDRduration = 5;           /* GDR refresh duration                   */
    priv_->dynParams.GDRinterval = 30;           /* Interval between GDR refresh           */
    priv_->dynParams.CVBRminbitrate = 0;   /* Reserved */
    priv_->dynParams.disableMVDCostFactor = 0;  /* Reserved */
    priv_->dynParams.putDataGetSpaceFxn = 0;   /* Pointer to callback module */
    priv_->dynParams.dataSyncHandle = 0;  /* Handle to datSyncDesc */

    qDebug("Calling VIDENC1_create");
    priv_->hEncode = VIDENC1_create(priv_->hEngine, codecName, (IVIDENC1_Params*) &params);
    if (priv_->hEncode == NULL)
        qFatal("Failed to open video encode algorithm: %s (0x%x)", codecName, Engine_getLastError(priv_->hEngine));

    /* Set video encoder dynamic parameters */
    VIDENC1_Status encStatus;
    encStatus.size = sizeof(VIDENC1_Status);
    encStatus.data.buf = NULL;

    qDebug("Calling VIDENC1_control(XDM_SETPARAMS)");
    XDAS_Int32 status = VIDENC1_control(priv_->hEncode, XDM_SETPARAMS, (IVIDENC1_DynamicParams*) &priv_->dynParams, &encStatus);
    if (status != VIDENC1_EOK)
        qFatal("XDM_SETPARAMS failed, status=%d, extendedError=%x (%s)",
               (int) status, (unsigned int) encStatus.extendedError,
               qPrintable(TIErrorToString(encStatus.extendedError)));

    /* Get buffer information from video encoder */
    qDebug("Calling VIDENC1_control(XDM_GETBUFINFO)");
    status = VIDENC1_control(priv_->hEncode, XDM_GETBUFINFO, (IVIDENC1_DynamicParams*) &priv_->dynParams, &encStatus);
    if (status != VIDENC1_EOK)
        qFatal("XDM_GETBUFINFO control failed status=%d", (int) status);

    if (encStatus.bufInfo.minNumInBufs != 2 ||
            encStatus.bufInfo.minNumOutBufs > 2)
        qFatal("Code expects 2 input buffers (Y and CbCr) and 1 or 2 output buffers (no b-frames). If this changes, something might fail");

    CMEM_AllocParams alloc_params;
    alloc_params.type = CMEM_POOL;
    alloc_params.flags = CMEM_CACHED;
    alloc_params.alignment = 32;

    for (int i = 0; i < encStatus.bufInfo.minNumOutBufs; i++) {
        priv_->encoderOutBufSizeArray[i] = encStatus.bufInfo.minOutBufSize[i];
        priv_->encoderOutBufArray[i] = (XDAS_Int8*) CMEM_alloc(priv_->encoderOutBufSizeArray[i], &alloc_params);
        if (!priv_->encoderOutBufArray[i])
            qFatal("Failed to allocate encoder output buffer of size %lu", priv_->encoderOutBufSizeArray[i]);
    }
    priv_->encoderOutBufDesc.bufs = priv_->encoderOutBufArray;
    priv_->encoderOutBufDesc.bufSizes = priv_->encoderOutBufSizeArray;
    priv_->encoderOutBufDesc.numBufs = encStatus.bufInfo.minNumOutBufs;
}

void H264Encoder::freeEncoder()
{
    // Free up the encoder resources
    VIDENC1_delete(priv_->hEncode);
    Engine_close(priv_->hEngine);

    // Free up the cmem allocations
    CMEM_AllocParams alloc_params;
    alloc_params.type = CMEM_POOL;
    alloc_params.flags = CMEM_CACHED;
    alloc_params.alignment = 32;

    for (int i = 0; i < priv_->encoderOutBufDesc.numBufs; i++) {
        CMEM_free(priv_->encoderOutBufArray[i], &alloc_params);
        priv_->encoderOutBufSizeArray[i] = 0;
        priv_->encoderOutBufArray[i] = 0;
    }
    priv_->encoderOutBufDesc.numBufs = 0;
}

void H264Encoder::frameReady(const VideoFramePtr &frame)
{
    // Check if the frame width and height changed
    // NOTE: This doesn't seem to work???
    if (priv_->dynParams.videncDynamicParams.inputHeight != frame->height ||
            priv_->dynParams.videncDynamicParams.inputWidth != frame->width) {
        priv_->dynParams.videncDynamicParams.inputHeight = frame->height;
        priv_->dynParams.videncDynamicParams.inputWidth = frame->width;

        /* Update video encoder dynamic parameters */
        VIDENC1_Status encStatus;
        encStatus.size = sizeof(VIDENC1_Status);
        encStatus.data.buf = NULL;

        XDAS_Int32 status = VIDENC1_control(priv_->hEncode, XDM_SETPARAMS, (IVIDENC1_DynamicParams*) &priv_->dynParams, &encStatus);
        if (status != VIDENC1_EOK)
            qFatal("XDM_SETPARAMS failed, status=%d", (int) status);
    }

    /* Set up the codec buffer dimensions */
    IVIDEO1_BufDescIn       inBufDesc;
    inBufDesc.frameWidth = frame->width;
    inBufDesc.frameHeight = frame->height;
    inBufDesc.framePitch = 0; // This is ignored by the encoder.
                              // dynParams.videncDynamicParams.captureWidth is what is really used.

    inBufDesc.numBufs = 2;
    inBufDesc.bufDesc[0].buf        = (XDAS_Int8*) frame->luminance;
    inBufDesc.bufDesc[0].bufSize    = frame->luminanceSize;
    inBufDesc.bufDesc[1].buf        = (XDAS_Int8*) frame->chrominance;
    inBufDesc.bufDesc[1].bufSize    = frame->chrominanceSize;

    IH264VENC_InArgs inArgs;
    inArgs.videncInArgs.size = sizeof(IH264VENC_InArgs);
    inArgs.videncInArgs.inputID = frame->id;
    if (inArgs.videncInArgs.inputID == 0)
        qFatal("Encoder will give an error if the inputID is 0");

    /* topFieldFirstFlag is hardcoded. Used only for interlaced content */
    inArgs.videncInArgs.topFieldFirstFlag            = 1;

    // Not using VUI so no timestamp
    inArgs.timeStamp = 0;

    // Not using the following
    memset(&inArgs.roiParameters, 0, sizeof(inArgs.roiParameters));
    inArgs.numOutputDataUnits = 0;

    // No SEI data for the decoder
    inArgs.insertUserData = 0;
    inArgs.lengthUserData = 0;

    IH264VENC_OutArgs outArgs;
    memset(&outArgs, 0, sizeof(outArgs));
    outArgs.videncOutArgs.size = sizeof(outArgs);

    // Get rid of any old cached encoded data
    CMEM_cacheInv(priv_->encoderOutBufArray[0], priv_->encoderOutBufSizeArray[0]);

    // NOTE: VIDENC1_process hangs if too many changes to kernel and TI code not rebuilt
    /* Encode video buffer */
    XDAS_Int32 status = VIDENC1_process(priv_->hEncode, &inBufDesc, &priv_->encoderOutBufDesc, &inArgs.videncInArgs,
                             &outArgs.videncOutArgs);

    if (status != VIDENC1_EOK) {
        qFatal("VIDENC1_process() failed with error (%d ext: %s 0x%x)",
                (Int)status,
                qPrintable(TIErrorToString(outArgs.videncOutArgs.extendedError)),
                (Uns) outArgs.videncOutArgs.extendedError);
    }

    if (inArgs.videncInArgs.inputID != outArgs.videncOutArgs.outputID)
        qFatal("Code assumes that buffer submitted is released after VIDENC1_process call. in=%d, out=%d",
               (int) inArgs.videncInArgs.inputID, (int) outArgs.videncOutArgs.outputID);

#if 1
    qDebug("Frame %d: %d bytes, type %d",
           frame->id,
           (int) outArgs.videncOutArgs.encodedBuf.bufSize,
           (int) outArgs.videncOutArgs.encodedFrameType);
#endif

#ifdef SAVE_TO_FILE
    fwrite(outArgs.videncOutArgs.encodedBuf.buf, 1, outArgs.videncOutArgs.encodedBuf.bufSize, priv_->encodeOutFile);
#endif
}

