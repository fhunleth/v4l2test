#include "tiutilities.h"
#include <xdc/std.h>
#include <ti/sdo/ce/osal/Global.h>
#include <ti/sdo/ce/osal/Memory.h>
#include <ti/sdo/ce/osal/Sem.h>
#include <ti/sdo/utils/trace/gt.h>

#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/video1/videnc1.h>
#include <ti/sdo/codecs/h264enc/ih264venc.h>
#include <ti/sdo/linuxutils/cmem/include/cmem.h>

struct TIErrorToStringEntry
{
    XDAS_Int32 code;
    const char *string;
};

static TIErrorToStringEntry tiErrors[] = {
    { IH264VENC_ERR_MAXWIDTH, "IH264VENC_ERR_MAXWIDTH"},
    { IH264VENC_ERR_MAXHEIGHT, "IH264VENC_ERR_MAXHEIGHT"},
    { IH264VENC_ERR_ENCODINGPRESET, "IH264VENC_ERR_ENCODINGPRESET"},
    { IH264VENC_ERR_RATECONTROLPRESET, "IH264VENC_ERR_RATECONTROLPRESET"},
    { IH264VENC_ERR_MAXFRAMERATE, "IH264VENC_ERR_MAXFRAMERATE"},
    { IH264VENC_ERR_MAXBITRATE, "IH264VENC_ERR_MAXBITRATE"},
    { IH264VENC_ERR_DATAENDIANNESS, "IH264VENC_ERR_DATAENDIANNESS"},
    { IH264VENC_ERR_INPUTCHROMAFORMAT, "IH264VENC_ERR_INPUTCHROMAFORMAT"},
    { IH264VENC_ERR_INPUTCONTENTTYPE, "IH264VENC_ERR_INPUTCONTENTTYPE"},
    { IH264VENC_ERR_RECONCHROMAFORMAT, "IH264VENC_ERR_RECONCHROMAFORMAT"},
    { IH264VENC_ERR_INPUTWIDTH, "IH264VENC_ERR_INPUTWIDTH"},
    { IH264VENC_ERR_INPUTHEIGHT, "IH264VENC_ERR_INPUTHEIGHT"},
    { IH264VENC_ERR_MAX_MBS_IN_FRM_LIMIT_EXCEED, "IH264VENC_ERR_MAX_MBS_IN_FRM_LIMIT_EXCEED"},
    { IH264VENC_ERR_TARGETFRAMERATE, "IH264VENC_ERR_TARGETFRAMERATE"},
    { IH264VENC_ERR_TARGETBITRATE, "IH264VENC_ERR_TARGETBITRATE"},
    { IH264VENC_ERR_PROFILEIDC, "IH264VENC_ERR_PROFILEIDC"},
    { IH264VENC_ERR_LEVELIDC, "IH264VENC_ERR_LEVELIDC"},
    { IH264VENC_ERR_ENTROPYMODE_IN_BP, "IH264VENC_ERR_ENTROPYMODE_IN_BP"},
    { IH264VENC_ERR_TRANSFORM8X8FLAGINTRA_IN_BP_MP, "IH264VENC_ERR_TRANSFORM8X8FLAGINTRA_IN_BP_MP"},
    { IH264VENC_ERR_TRANSFORM8X8FLAGINTER_IN_BP_MP, "IH264VENC_ERR_TRANSFORM8X8FLAGINTER_IN_BP_MP"},
    { IH264VENC_ERR_SEQSCALINGFLAG_IN_BP_MP, "IH264VENC_ERR_SEQSCALINGFLAG_IN_BP_MP"},
    { IH264VENC_ERR_ASPECTRATIOX, "IH264VENC_ERR_ASPECTRATIOX"},
    { IH264VENC_ERR_ASPECTRATIOY, "IH264VENC_ERR_ASPECTRATIOY"},
    { IH264VENC_ERR_PIXELRANGE, "IH264VENC_ERR_PIXELRANGE"},
    { IH264VENC_ERR_TIMESCALE, "IH264VENC_ERR_TIMESCALE"},
    { IH264VENC_ERR_NUMUNITSINTICKS, "IH264VENC_ERR_NUMUNITSINTICKS"},
    { IH264VENC_ERR_ENABLEVUIPARAMS, "IH264VENC_ERR_ENABLEVUIPARAMS"},
    { IH264VENC_ERR_RESETHDVICPEVERYFRAME, "IH264VENC_ERR_RESETHDVICPEVERYFRAME"},
    { IH264VENC_ERR_MEALGO, "IH264VENC_ERR_MEALGO"},
    { IH264VENC_ERR_UNRESTRICTEDMV, "IH264VENC_ERR_UNRESTRICTEDMV"},
    { IH264VENC_ERR_ENCQUALITY, "IH264VENC_ERR_ENCQUALITY"},
    { IH264VENC_ERR_ENABLEARM926TCM, "IH264VENC_ERR_ENABLEARM926TCM"},
    { IH264VENC_ERR_ENABLEDDRBUFF, "IH264VENC_ERR_ENABLEDDRBUFF"},
    { IH264VENC_ERR_LEVEL_NOT_FOUND, "IH264VENC_ERR_LEVEL_NOT_FOUND"},
    { IH264VENC_ERR_REFFRAMERATE_MISMATCH, "IH264VENC_ERR_REFFRAMERATE_MISMATCH"},
    { IH264VENC_ERR_INTRAFRAMEINTERVAL, "IH264VENC_ERR_INTRAFRAMEINTERVAL"},
    { IH264VENC_ERR_GENERATEHEADER, "IH264VENC_ERR_GENERATEHEADER"},
    { IH264VENC_ERR_FORCEFRAME, "IH264VENC_ERR_FORCEFRAME"},
    { IH264VENC_ERR_RCALGO, "IH264VENC_ERR_RCALGO"},
    { IH264VENC_ERR_INTRAFRAMEQP, "IH264VENC_ERR_INTRAFRAMEQP"},
    { IH264VENC_ERR_INTERPFRAMEQP, "IH264VENC_ERR_INTERPFRAMEQP"},
    { IH264VENC_ERR_RCQMAX, "IH264VENC_ERR_RCQMAX"},
    { IH264VENC_ERR_RCQMIN, "IH264VENC_ERR_RCQMIN"},
    { IH264VENC_ERR_RCIQMAX, "IH264VENC_ERR_RCIQMAX"},
    { IH264VENC_ERR_RCIQMIN, "IH264VENC_ERR_RCIQMIN"},
    { IH264VENC_ERR_INITQ, "IH264VENC_ERR_INITQ"},
    { IH264VENC_ERR_MAXDELAY, "IH264VENC_ERR_MAXDELAY"},
    { IH264VENC_ERR_LFDISABLEIDC, "IH264VENC_ERR_LFDISABLEIDC"},
    { IH264VENC_ERR_ENABLEBUFSEI, "IH264VENC_ERR_ENABLEBUFSEI"},
    { IH264VENC_ERR_ENABLEPICTIMSEI, "IH264VENC_ERR_ENABLEPICTIMSEI"},
    { IH264VENC_ERR_SLICESIZE, "IH264VENC_ERR_SLICESIZE"},
    { IH264VENC_ERR_INTRASLICENUM, "IH264VENC_ERR_INTRASLICENUM"},
    { IH264VENC_ERR_AIRRATE, "IH264VENC_ERR_AIRRATE"},
    { IH264VENC_ERR_MEMULTIPART, "IH264VENC_ERR_MEMULTIPART"},
    { IH264VENC_ERR_INTRATHRQF, "IH264VENC_ERR_INTRATHRQF"},
    { IH264VENC_ERR_PERCEPTUALRC, "IH264VENC_ERR_PERCEPTUALRC"},
    { IH264VENC_ERR_IDRFRAMEINTERVAL, "IH264VENC_ERR_IDRFRAMEINTERVAL"},
    { IH264VENC_ERR_ENABLEROI, "IH264VENC_ERR_ENABLEROI"},
    { IH264VENC_ERR_MAXBITRATE_CVBR, "IH264VENC_ERR_MAXBITRATE_CVBR"},
    { IH264VENC_ERR_CVBR_SENSITIVITY, "IH264VENC_ERR_CVBR_SENSITIVITY"},
    { IH264VENC_ERR_CVBR_MAX_CMPX_INT, "IH264VENC_ERR_CVBR_MAX_CMPX_INT"},
    { IH264VENC_ERR_MVSADOUTFLAG, "IH264VENC_ERR_MVSADOUTFLAG"},
    { IH264VENC_ERR_MAXINTERFRAMEINTERVAL, "IH264VENC_ERR_MAXINTERFRAMEINTERVAL"},
    { IH264VENC_ERR_CAPTUREWIDTH, "IH264VENC_ERR_CAPTUREWIDTH"},
    { IH264VENC_ERR_INTERFRAMEINTERVAL, "IH264VENC_ERR_INTERFRAMEINTERVAL"},
    { IH264VENC_ERR_MBDATAFLAG, "IH264VENC_ERR_MBDATAFLAG"},
    { IH264VENC_ERR_IVIDENC1_DYNAMICPARAMS_SIZE_IN_CORRECT, "IH264VENC_ERR_IVIDENC1_DYNAMICPARAMS_SIZE_IN_CORRECT"},
    { IH264VENC_ERR_IVIDENC1_PROCESS_ARGS_NULL, "IH264VENC_ERR_IVIDENC1_PROCESS_ARGS_NULL"},
    { IH264VENC_ERR_IVIDENC1_INARGS_SIZE, "IH264VENC_ERR_IVIDENC1_INARGS_SIZE"},
    { IH264VENC_ERR_IVIDENC1_OUTARGS_SIZE, "IH264VENC_ERR_IVIDENC1_OUTARGS_SIZE"},
    { IH264VENC_ERR_IVIDENC1_INARGS_INPUTID, "IH264VENC_ERR_IVIDENC1_INARGS_INPUTID"},
    { IH264VENC_ERR_IVIDENC1_INARGS_TOPFIELDFIRSTFLAG, "IH264VENC_ERR_IVIDENC1_INARGS_TOPFIELDFIRSTFLAG"},
    { IH264VENC_ERR_IVIDENC1_INBUFS, "IH264VENC_ERR_IVIDENC1_INBUFS"},
    { IH264VENC_ERR_IVIDENC1_INBUFS_BUFDESC, "IH264VENC_ERR_IVIDENC1_INBUFS_BUFDESC"},
    { IH264VENC_ERR_IVIDENC1_OUTBUFS, "IH264VENC_ERR_IVIDENC1_OUTBUFS"},
    { IH264VENC_ERR_IVIDENC1_OUTBUFS_NULL, "IH264VENC_ERR_IVIDENC1_OUTBUFS_NULL"},
    { IH264VENC_ERR_INTERLACE_IN_BP, "IH264VENC_ERR_INTERLACE_IN_BP"},
    { IH264VENC_ERR_RESERVED, "IH264VENC_ERR_RESERVED"},
    { IH264VENC_ERR_INSERTUSERDATA, "IH264VENC_ERR_INSERTUSERDATA"},
    { IH264VENC_ERR_ROIPARAM, "IH264VENC_ERR_ROIPARAM"},
    { IH264VENC_ERR_LENGTHUSERDATA, "IH264VENC_ERR_LENGTHUSERDATA"},
    { IH264VENC_ERR_PROCESS_CALL, "IH264VENC_ERR_PROCESS_CALL"},
    { IH264VENC_ERR_HANDLE_NULL, "IH264VENC_ERR_HANDLE_NULL"},
    { IH264VENC_ERR_INCORRECT_HANDLE, "IH264VENC_ERR_INCORRECT_HANDLE"},
    { IH264VENC_ERR_MEMTAB_NULL, "IH264VENC_ERR_MEMTAB_NULL"},
    { IH264VENC_ERR_IVIDENC1_INITPARAMS_SIZE, "IH264VENC_ERR_IVIDENC1_INITPARAMS_SIZE"},
    { IH264VENC_ERR_MEMTABS_BASE_NULL, "IH264VENC_ERR_MEMTABS_BASE_NULL"},
    { IH264VENC_ERR_MEMTABS_BASE_NOT_ALIGNED, "IH264VENC_ERR_MEMTABS_BASE_NOT_ALIGNED"},
    { IH264VENC_ERR_MEMTABS_SIZE, "IH264VENC_ERR_MEMTABS_SIZE"},
    { IH264VENC_ERR_MEMTABS_ATTRS, "IH264VENC_ERR_MEMTABS_ATTRS"},
    { IH264VENC_ERR_MEMTABS_SPACE, "IH264VENC_ERR_MEMTABS_SPACE"},
    { IH264VENC_ERR_MEMTABS_OVERLAP, "IH264VENC_ERR_MEMTABS_OVERLAP"},
    { IH264VENC_ERR_CODEC_INACTIVE, "IH264VENC_ERR_CODEC_INACTIVE"},
    { IH264VENC_WARN_LEVELIDC, "IH264VENC_WARN_LEVELIDC"},
    { IH264VENC_WARN_RATECONTROLPRESET, "IH264VENC_WARN_RATECONTROLPRESET"},
    { IH264VENC_WARN_H241_SLICE_SIZE_EXCEEDED, "IH264VENC_WARN_H241_SLICE_SIZE_EXCEEDED"},
    { IH264VENC_ERR_STATUS_BUF, "IH264VENC_ERR_STATUS_BUF"},
    { IH264VENC_ERR_METADATAFLAG, "IH264VENC_ERR_METADATAFLAG"},
    { 0, 0 }
};

QString TIErrorToString(int errorCode)
{
    if(0 == errorCode)
        return "Ok";

    QString msg;
    if(XDM_ISFATALERROR(errorCode))
        msg += "Fatal Error";

    if(XDM_ISUNSUPPORTEDPARAM(errorCode))
        msg += "Unsupported Param Error";

    if(XDM_ISUNSUPPORTEDINPUT(errorCode))
        msg += "Unsupported Input Error";
    msg += ": ";

    TIErrorToStringEntry *err = tiErrors;
    while (err->string != 0) {
        if (err->code == errorCode)
            break;
    }

    if (err->string != 0)
        msg += err->string;
    else
        msg += "Unknown Error code";
    return msg;
}
