#include "davincipreviewer.h"

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <media/davinci/imp_previewer.h>
#include <media/davinci/dm365_ipipe.h>

#include <QDebug>

DavinciPreviewer::DavinciPreviewer() :
    preview_fd(-1)
{
}

DavinciPreviewer::~DavinciPreviewer()
{
    close();
}

void DavinciPreviewer::init()
{
    unsigned int oper_mode;
    unsigned int user_mode;
    struct prev_channel_config prev_chan_config;
    struct prev_continuous_config prev_cont_config; // continuous mode

    preview_fd = open("/dev/davinci_previewer", O_RDWR);
    if (preview_fd <= 0)
            qFatal("Cannot open previewer device");

    user_mode = IMP_MODE_CONTINUOUS;
    if (ioctl(preview_fd, PREV_S_OPER_MODE, &user_mode) < 0)
        qFatal("Can't set operation mode");

    if (ioctl(preview_fd, PREV_G_OPER_MODE, &oper_mode) < 0)
        qFatal("Can't get operation mode");

    if (oper_mode == user_mode)
        qDebug("Operating mode changed successfully to continuous in previewer");
    else
        qFatal("failed to set mode to continuous in resizer");

    qDebug("Setting default configuration in previewer");
    prev_chan_config.oper_mode = IMP_MODE_CONTINUOUS;
    prev_chan_config.len = 0;
    prev_chan_config.config = NULL; /* to set defaults in driver */
    if (ioctl(preview_fd, PREV_S_CONFIG, &prev_chan_config) < 0)
        qFatal("Error in setting default configuration");

    qDebug("default configuration setting in previewer successfull");
    prev_chan_config.oper_mode = IMP_MODE_CONTINUOUS;
    prev_chan_config.len = sizeof(struct prev_continuous_config);
    prev_chan_config.config = &prev_cont_config;

    if (ioctl(preview_fd, PREV_G_CONFIG, &prev_chan_config) < 0)
            qFatal("Error in getting configuration from driver");

    prev_cont_config.input.colp_elep = IPIPE_BLUE;
    prev_cont_config.input.colp_elop = IPIPE_GREEN_BLUE;
    prev_cont_config.input.colp_olep = IPIPE_GREEN_RED;
    prev_cont_config.input.colp_olop = IPIPE_RED,

    prev_chan_config.oper_mode = IMP_MODE_CONTINUOUS;
    prev_chan_config.len = sizeof(struct prev_continuous_config);
    prev_chan_config.config = &prev_cont_config;

    if (ioctl(preview_fd, PREV_S_CONFIG, &prev_chan_config) < 0)
        qFatal("Error in setting default configuration");

    qDebug("previewer initialized");
}

void DavinciPreviewer::close()
{
    if (preview_fd != -1) {
        ::close(preview_fd);
        preview_fd = -1;
    }
}
