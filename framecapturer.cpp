#include "framecapturer.h"
#include "videoframe.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <linux/videodev2.h>
#include <ti/sdo/linuxutils/cmem/include/cmem.h>

#include <QSocketNotifier>
#include <QDebug>
#include <QMetaObject>
#include <QThread>

#include <media/davinci/dm365_ccdc.h>
#include <media/davinci/vpfe_capture.h>

// Comment in to process frame directly from the IPIPEIF from
// the camera. Comment out to have the frames go through the
// rest of the video hardware and through the resizer
//#define CAPTURE_RAW_FRAMES

#define VPFE_FRAME_OUTPUT_WIDTH  FRAME_CAPTURE_MAX_WIDTH
#define VPFE_FRAME_OUTPUT_HEIGHT FRAME_CAPTURE_MAX_HEIGHT
#define VPFE_FRAME_OUTPUT_LUMINANCE_SIZE_IN_BYTES (VPFE_FRAME_OUTPUT_WIDTH * VPFE_FRAME_OUTPUT_HEIGHT)
#define VPFE_FRAME_OUTPUT_SIZE_IN_BYTES (VPFE_FRAME_OUTPUT_WIDTH * VPFE_FRAME_OUTPUT_HEIGHT * 3 / 2)

#define CAPTURE_BUFS_TO_CAPTURE_DRIVER  5
#define CAPTURE_THREAD_NUM_BUFS  CAPTURE_BUFS_TO_CAPTURE_DRIVER

#define ALIGN(x, y)	(((x + (y-1))/y)*y)

#define CAPTURE_DEVICE      "/dev/video0"
#define CLEAR(x)            memset (&(x), 0, sizeof (x))

static VideoFrame frames[CAPTURE_THREAD_NUM_BUFS];

FrameCapturer::FrameCapturer(const QByteArray &cameraName, QObject *parent) :
    QObject(parent),
    cameraName_(cameraName),
    frameCnt(0),
    framesOutToEncoder_(0)
{
}

FrameCapturer *FrameCapturer::startInThread(const QByteArray &cameraName, QThread *thread)
{
    // Register the metatypes that we use since this is a convenient location.
    qRegisterMetaType<VideoFramePtr>("VideoFramePtr");
    qRegisterMetaType<VideoFrame*>("VideoFrame*");

    FrameCapturer *capturer = new FrameCapturer(cameraName);
    capturer->moveToThread(thread);
    capturer->allocateCmemBuffers();

    // Start the capture loop going when the thread is started.
    // This lets the user make the rest of the connections.
    QMetaObject::invokeMethod(capturer, "startCapturing");

    return capturer;
}

void FrameCapturer::changeSettings(const FrameCaptureSettings &newSettings)
{
    // Check that we're in the capture thread. If not, redispatch.
    if (QThread::currentThread() != this->thread()) {
        QMetaObject::invokeMethod(this, "changeSettings", Q_ARG(FrameCaptureSettings, newSettings));
        return;
    }

    // TODO - validate the settings

    // Stop

    // Apply settings

    // Start


    emit settingsChanged(newSettings);
}


void FrameCapturer::allocateCmemBuffers()
{
    CMEM_AllocParams alloc_params;
    int i;

    alloc_params.type = CMEM_POOL;
    alloc_params.flags = CMEM_CACHED; //CMEM_NONCACHED;
    alloc_params.alignment = 32;
    for (i = 0; i < CAPTURE_THREAD_NUM_BUFS; i++) {
        // Allocate the memory
        VideoFrame *frame = &frames[i];

        frame->id = 1 + i;
        frame->capturer = this;
        frame->width = FRAME_CAPTURE_MAX_WIDTH;
        frame->height = FRAME_CAPTURE_MAX_HEIGHT;
        frame->luminanceSize = VPFE_FRAME_OUTPUT_LUMINANCE_SIZE_IN_BYTES;
        frame->chrominanceSize = frame->luminanceSize / 2;
        frame->totalFrameSize = frame->luminanceSize + frame->chrominanceSize;
        frame->luminance = (unsigned char *) CMEM_alloc(frame->totalFrameSize, &alloc_params);
        frame->chrominance = frame->luminance + frame->luminanceSize;
        if (!frame->luminance)
            qFatal("Failed to allocate cmem buffer");

        CLEAR(frame->v4l2buf);
        frame->v4l2buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        frame->v4l2buf.memory = V4L2_MEMORY_USERPTR;
        frame->v4l2buf.index = i;
        frame->v4l2buf.length = frame->totalFrameSize;
        frame->v4l2buf.m.userptr = (unsigned long) frame->luminance;
    }
}

void FrameCapturer::startCapturing()
{
    int i;

#ifndef CAPTURE_RAW_FRAMES
    // Configure the resizer based on the settings
    resizer_.setOrientation(0);

    resizer_.commitSettingsAndStart();
    previewer_.init();
#endif

    // Initialize all of the frames to black.
    for (i = 0; i < CAPTURE_THREAD_NUM_BUFS; i++) {
        VideoFrame *frame = &frames[i];

        // Initialize all memory to known values. This removes random noise on edge pixels not set
        // by the capture hardware and makes memory dumps nicer to look at.
        memset(frame->luminance, 0, frame->luminanceSize);
        memset(frame->chrominance, 0x80, frame->chrominanceSize);

        // Send everything that's still in the cache to DRAM and remove the
        // cache entries.
        CMEM_cacheWbInv(frame->luminance, frame->totalFrameSize);
    }

    // Initialize and send buffers to the imager
    init_camera_capture();
}

void FrameCapturer::stopCapturing()
{
    // TODO: wait for all frames to be returned from the encoder.

    notifier_->deleteLater();
    notifier_ = 0;

    cleanup_capture();

#ifndef CAPTURE_RAW_FRAMES
    previewer_.close();
    resizer_.close();
#endif
    capt_fd = -1;
}

void FrameCapturer::set_data_format()
{
    struct v4l2_format fmt;
    struct v4l2_input input;
    int temp_input;
    int found = 0;

    // first set the input to the Camera
    input.type = V4L2_INPUT_TYPE_CAMERA;
    input.index = 0;
    qDebug("Detecting if driver supports camera");
    while (0 == ioctl(capt_fd,VIDIOC_ENUMINPUT, &input)) {
        if (qstrcmp(cameraName_.data(), (const char *) input.name) == 0) {
            found = 1;
            break;
        }
        input.index++;
    }

    if (!found)
        qFatal("Can't find input %s from the capture driver", cameraName_.data());

    if (-1 == ioctl (capt_fd, VIDIOC_G_INPUT, &temp_input))
        qFatal("error: InitDevice:ioctl:VIDIOC_G_INPUT");

    qDebug("Going to select input %d (%s: status=0x%08x). Input was %d", input.index, input.name, input.status, temp_input);
    if (-1 == ioctl (capt_fd, VIDIOC_S_INPUT, &input.index))
        qFatal("InitDevice:ioctl:VIDIOC_S_INPUT");

    if (-1 == ioctl (capt_fd, VIDIOC_G_INPUT, &temp_input))
        qFatal("error: InitDevice:ioctl:VIDIOC_G_INPUT");

    if (temp_input == (int) input.index)
        qDebug("InitDevice:ioctl:VIDIOC_G_INPUT, selected input %s ",
                input.name);
    else
        qFatal("InitDevice:ioctl:VIDIOC_G_INPUT, Couldn't select input. Got %d but wanted %d.", temp_input, input.index);

    CLEAR(fmt);
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.field = V4L2_FIELD_NONE;

    QSize defaultEncodeDimensions;
    maxDimensions_ = QSize(COLOR_CAMERA_WIDTH, COLOR_CAMERA_HEIGHT);
    defaultEncodeDimensions = QSize(1920, 1080);
    encodeDimensions_ = defaultEncodeDimensions;
    encodeDimensions_ = encodeDimensions_.boundedTo(maxDimensions_);

    // Encode dimensions must be a multiple of 16.
    encodeDimensions_.setWidth(encodeDimensions_.width() & ~0xf);
    encodeDimensions_.setHeight(encodeDimensions_.height() & ~0xf);

    fmt.fmt.pix.width = encodeDimensions_.width();
    fmt.fmt.pix.height = encodeDimensions_.height();
    // For simplicity on the encoder side, we always have a stride of the max width (rounded)
    // NOTE: For some reason, the bytes per line is ignored. Need to investigate, but might
    // not be a big deal.
    fmt.fmt.pix.bytesperline = ALIGN(FRAME_CAPTURE_MAX_WIDTH, 32);
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_NV12;

    qDebug("driver parameters for S_FMT");
    qDebug("fmt.fmt.pix.width  = %d", fmt.fmt.pix.width);
    qDebug("fmt.fmt.pix.height = %d", fmt.fmt.pix.height);
    if (-1 == ioctl(capt_fd, VIDIOC_S_FMT, &fmt))
        qFatal("set_data_format:ioctl:VIDIOC_S_FMT");

    if (-1 == ioctl(capt_fd, VIDIOC_G_FMT, &fmt))
        qFatal("set_data_format:ioctl:VIDIOC_G_FMT:");

    captureBytesPerLine_ = fmt.fmt.pix.bytesperline;

    qDebug("FrameCapturer: max dimensions are width:%d height:%d",
           maxDimensions_.width(), maxDimensions_.height());
    qDebug("FrameCapturer: bytesperline = %d, sizeimage = %d",
           fmt.fmt.pix.bytesperline, fmt.fmt.pix.sizeimage);

    setSourceRect(QRect(0, 0, maxDimensions_.width(), maxDimensions_.height()));
}

void FrameCapturer::setSourceRect(const QRect &rect)
{
    QRect maxSourceRect(0, 0, maxDimensions_.width(), maxDimensions_.height());
    QRect srcRect = rect;
    if (srcRect.isEmpty())
        srcRect = maxSourceRect;
    else
        srcRect = srcRect.intersected(maxSourceRect);

    v4l2_crop crop;
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c.left = srcRect.x();
    crop.c.top = srcRect.y();
    crop.c.width = srcRect.width();
    crop.c.height = srcRect.height();
    qDebug("Cropping input frame to %d %d %d %d", crop.c.left, crop.c.top, crop.c.width, crop.c.height);
    if (-1 == ioctl(capt_fd, VIDIOC_S_CROP, &crop))
        qWarning("set_data_format:ioctl:VIDIOC_S_CROP:%s", strerror(errno));
}

QSize FrameCapturer::encodeDimensions() const
{
    return encodeDimensions_;
}

QSize FrameCapturer::maxDimensions() const
{
    return maxDimensions_;
}

int FrameCapturer::captureBytesPerLine() const
{
    return captureBytesPerLine_;
}

void FrameCapturer::initCaptureBuffers()
{
    struct v4l2_requestbuffers req;

    CLEAR(req);
    req.count = CAPTURE_THREAD_NUM_BUFS;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_USERPTR;

    if (-1 == ioctl(capt_fd, VIDIOC_REQBUFS, &req)) {
            qFatal("InitCaptureBuffers:ioctl:VIDIOC_REQBUFS");
    }

    if (req.count != CAPTURE_THREAD_NUM_BUFS) {
            qFatal("VIDIOC_REQBUFS failed for capture");
    }
}

int FrameCapturer::start_capture_streaming()
{
    notifier_ = new QSocketNotifier(capt_fd, QSocketNotifier::Read, this);
    connect(notifier_, SIGNAL(activated(int)), SLOT(processFrame()));

    // Make sure that the frames all have the requested width and height.
    for (int i = 0; i < CAPTURE_THREAD_NUM_BUFS; i++) {
        VideoFrame *frame = &frames[i];

        frame->width = encodeDimensions_.width();
        frame->height = encodeDimensions_.height();

        // Update the chrominance and luminance sizes and offsets
        // now that we know the true width and height.

        // TODO: THE INTENTION WAS THAT THE STRIDE REMAIN THE MAX
        // WIDTH, SO THAT IF THE SIZE CHANGED MIDFRAME, THE GLITCH
        // WOULD LOOK LESS WEIRD. HOWEVER, THE ENCODER DOESN'T SEEM
        // TO BE ABLE TO CHANGE DYNAMICALLY (OR THE DECODER DOESN"T
        // HANDLE IT.
        frame->luminanceSize = frame->height * frame->width;
        frame->chrominance = frame->luminance + frame->luminanceSize;
        frame->chrominanceSize = frame->luminanceSize / 2;
    }

    // Submit the frames to the driver.
    for (int i = 0; i < CAPTURE_BUFS_TO_CAPTURE_DRIVER; i++) {
        if (ioctl(capt_fd, VIDIOC_QBUF, &frames[i].v4l2buf) < 0)
            qFatal("start_capture_streaming:ioctl:VIDIOC_QBUF: error");
    }

    ccdc_config_params_raw ccdcParams;
    if (-1 == ioctl(capt_fd, VPFE_CMD_G_CCDC_RAW_PARAMS, &ccdcParams))
        qFatal("StartStreaming:ioctl:VPFE_CMD_G_CCDC_RAW_PARAMS");

    // Set up the CFA matrix on ISIF (see similar setting on IPIPE)
    // bggr
    // gbrg
    // grbg
    // rggb
    ccdcParams.col_pat_field0.elep = CCDC_BLUE;
    ccdcParams.col_pat_field0.elop = CCDC_GREEN_BLUE;
    ccdcParams.col_pat_field0.olep = CCDC_GREEN_RED;
    ccdcParams.col_pat_field0.olop = CCDC_RED;
    ccdcParams.col_pat_field1.elep = CCDC_BLUE;
    ccdcParams.col_pat_field1.elop = CCDC_GREEN_BLUE;
    ccdcParams.col_pat_field1.olep = CCDC_GREEN_RED;
    ccdcParams.col_pat_field1.olop = CCDC_RED;

    if (-1 == ioctl(capt_fd, VPFE_CMD_S_CCDC_RAW_PARAMS, &ccdcParams))
        qFatal("StartStreaming:ioctl:VPFE_CMD_S_CCDC_RAW_PARAMS");

    // Start capturing...
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == ioctl(capt_fd, VIDIOC_STREAMON, &type)) {
        qFatal("StartStreaming:ioctl:VIDIOC_STREAMON:");
    } else
        qDebug("STREAMON Done");

#if 1
        /* Get a register just to see that we can do it. */
        {
                struct v4l2_dbg_register reg;
                CLEAR(reg);

                FILE *fp = fopen("camregs.txt", "w");
                for (int i = 0; i < 256; i++) {
                    reg.match.type = V4L2_CHIP_MATCH_I2C_ADDR;
                    reg.match.addr = 0x48;
                    reg.reg = i;
                    if (-1 == ioctl(capt_fd, VIDIOC_DBG_G_REGISTER, &reg)) {
                            perror("Can't get register. check that v4l2 advanced debug is on");
                    } else {
                        fprintf(fp, "R%d:0(R0x0%02x) = %d (0x%04x)\n",
                                (int) reg.reg, (int) reg.reg,
                                (int) reg.val, (int) reg.val);
                    }
                }
#if 1
                // Test pattern
                reg.reg = 0xa0;
                // Color field = 0x01
                // Horizontal gradient = 0x09
                // Vertical gradient = 0x11
                // Diagonal = 0x19
                // Classic = 0x21
                // Walking 1s = 0x29
                // Monochrome horizontal bars = 0x31
                // Monochrome vertical bars = 0x39
                // Vertical color bars = 0x41
                reg.val = 0x00; //0x41;
                if (-1 == ioctl(capt_fd, VIDIOC_DBG_S_REGISTER, &reg)) {
                    perror("Can't set register");
                }

#if 1
                // Green
                reg.reg = 0xa1;
                reg.val = 0xff;
                if (-1 == ioctl(capt_fd, VIDIOC_DBG_S_REGISTER, &reg)) {
                    perror("Can't set register");
                }
                // Red
                reg.reg = 0xa2;
                reg.val = 0xfff;
                if (-1 == ioctl(capt_fd, VIDIOC_DBG_S_REGISTER, &reg)) {
                    perror("Can't set register");
                }
                // Blue
                reg.reg = 0xa3;
                reg.val = 0x0;
                if (-1 == ioctl(capt_fd, VIDIOC_DBG_S_REGISTER, &reg)) {
                    perror("Can't set register");
                }
                // Color bar width
                reg.reg = 0xa4;
                reg.val = 0x09;
                if (-1 == ioctl(capt_fd, VIDIOC_DBG_S_REGISTER, &reg)) {
                    perror("Can't set register");
                }
#endif
#endif

        }
#endif

    return 0;
}

void FrameCapturer::init_camera_capture()
{
    struct v4l2_capability cap;

    if ((capt_fd = open(CAPTURE_DEVICE, O_RDWR, 0)) <= -1)
        qFatal("init_camera_capture: cannot open %s", CAPTURE_DEVICE);

    /*Is capture supported? */
    if (-1 == ioctl(capt_fd, VIDIOC_QUERYCAP, &cap))
        qFatal("init_camera_capture:ioctl:VIDIOC_QUERYCAP:");

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
        qFatal("InitDevice:capture is not supported on:%s",
               CAPTURE_DEVICE);

    /*is MMAP-IO supported? */
    if (!(cap.capabilities & V4L2_CAP_STREAMING))
        qFatal("InitDevice:IO method MMAP is not supported on:%s",
             CAPTURE_DEVICE);

    qDebug("setting data format");
    set_data_format();

    qDebug("initializing capture buffers");
    initCaptureBuffers();

#if 0
    struct v4l2_control control;
    CLEAR(control);
    control.id = V4L2_CID_EXPOSURE;
    control.value = 3000;
    if (-1 == ioctl(capt_fd, VIDIOC_S_CTRL, &control)) {
            qFatal("Cannot set exposure time");
    }
#endif

    start_capture_streaming();
}

void FrameCapturer::setOrientation(bool rotated180)
{
    stopCapturing();
    //settings_->setValue("orientation", rotated180 ? 180 : 0);
    startCapturing();
}

void FrameCapturer::cleanup_capture()
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == ioctl(capt_fd, VIDIOC_STREAMOFF, &type))
        qFatal("cleanup_capture :ioctl:VIDIOC_STREAMOFF");

    if (close(capt_fd) < 0)
        qFatal("Error in closing device");
}

void FrameCapturer::frameDone(VideoFrame *frame)
{
    framesOutToEncoder_--;
    Q_ASSERT(framesOutToEncoder_ >= 0);

    if (ioctl(capt_fd, VIDIOC_QBUF, &frame->v4l2buf) < 0)
        qFatal("frameDoneCallback:ioctl:VIDIOC_QBUF: error");
}

// Usually called from a different thread, so be careful
// on threading.
void FrameCapturer::frameDoneCallback(VideoFrame *frame)
{
    // Send a message to the FrameCapturerr's event loop
    // to return the frame.
    QMetaObject::invokeMethod(frame->capturer, "frameDone", Q_ARG(VideoFrame*, frame));
}

void FrameCapturer::processFrame()
{
    int ret;

    // Get the most recently captured frame.
    struct v4l2_buffer v4l2buf;
    v4l2buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    v4l2buf.memory = V4L2_MEMORY_USERPTR;
    // EINTR note: VIDIOC_DQBUF blocks so it can be interrupted. We don't care
    //             about interrupts, so just re-issue the call.
    while ((ret = ioctl(capt_fd, VIDIOC_DQBUF, &v4l2buf)) < 0 && errno == EINTR) ;
    if (ret < 0)
        qFatal("VIDIOC_DQBUF for capture failed. %s", strerror(errno));

    // Make sure that no stale data is left behind from a previously
    // captured frame.
    VideoFrame *frame = &frames[v4l2buf.index];
    Q_ASSERT(frame->luminance == v4l2buf.m.userptr);

    CMEM_cacheInv(frame->luminance, frame->totalFrameSize);
    framesOutToEncoder_++;
    if (framesOutToEncoder_ <= 2) {
        emit frameReady(VideoFramePtr(frame, frameDoneCallback));
    } else {
        qDebug("Dropping frame since behind -> %d", framesOutToEncoder_);

        // Encoder is behind. Recycle this frame immediately.
        frameDone(frame);
    }

#if 0 //def SAVE_FRAMES_TO_DISK
    // TODO - Move to class
    // Do something with the buffer
    {
        static int counter = 0;
        counter++;
        if ((counter % 15) == 0) {
            static int counter2 = 0;
            char filename[64];

            if (counter2 == 4) {
                counter2 = 0;
                qFatal("Stop...");
            }

            sprintf(filename, "capture%d.nv12", counter2++);
            FILE *fp = fopen(filename, "wb");
            // Invalidate the cache here. Safe since we only read frames. Don't
            // bother invalidating in the main loop since no other code touches
            // the frames.
    //See line above and think about it.        CMEM_cacheInv(frame->luminance, frame->totalFrameSize);
            fwrite(frame->luminance, 1, frame->totalFrameSize, fp);
            fclose(fp);
            qDebug("Saved frame to %s", filename);
        }
    }
#endif
}
