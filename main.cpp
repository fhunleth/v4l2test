#include <QApplication>
#include <QMetaType>
#include <QThread>

#include <xdc/std.h>
#include <ti/sdo/ce/osal/Memory.h>
#include <ti/sdo/ce/osal/Sem.h>
#include <ti/sdo/utils/trace/gt.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/linuxutils/cmem/include/cmem.h>

#include "framecapturer.h"
#include "h264encoder.h"

static void initTILibraries()
{
    CERuntime_init();
    GT_init();
    Memory_init();
    Sem_init();
    CMEM_init();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv, false);

    // Initialize all of the TI code
    initTILibraries();

    QThread *captureThread = new QThread();
    FrameCapturer *capturer = FrameCapturer::startInThread("Camera", captureThread);

    QThread *encoderThread = new QThread();
    H264Encoder *encoder = H264Encoder::startInThread(1280, 720, 3000000, encoderThread);
    QObject::connect(capturer, SIGNAL(frameReady(VideoFramePtr)), encoder, SLOT(frameReady(VideoFramePtr)));

    // Start the threads going
    encoderThread->start();
    captureThread->start();

    a.exec();

    delete capturer;
    return 0;
}
