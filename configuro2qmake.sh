#!/bin/sh
# Convert compiler arguments to qmake

export LC_ALL=C

export OPTERR=0
while getopts I:D: f
do
	case $f in
		I) echo "INCLUDEPATH += $OPTARG" ;;
		D) echo "DEFINES += $OPTARG" ;;
	esac
done

